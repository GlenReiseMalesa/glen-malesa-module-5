import 'package:flutter/material.dart';

MaterialButton longButtons(String title, var fun) {
  return MaterialButton(
      onPressed: fun,
      textColor: Colors.white,
      color: Colors.blue,
      child: SizedBox(
        width: double.infinity,
        child: Text(
          title,
          textAlign: TextAlign.center,
        ),
      ),
      height: 45,
      minWidth: 600,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))));
}
