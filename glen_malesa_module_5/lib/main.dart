// ignore_for_file: unused_element, prefer_const_constructors
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

import 'deco.dart';
import 'reusable_widgets.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter CRUD',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter CRUD Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final FirebaseFirestore firestore = FirebaseFirestore.instance;

  final _ageController = TextEditingController();
  final _fnController = TextEditingController();
  final _lnController = TextEditingController();

  void _create() async {
    var age = _ageController.text;
    var fn = _fnController.text;
    var ln = _lnController.text;

    try {
      await firestore.collection('user').doc('OrMkyIoptZ0W54UvaL3W').set({
        'FirstName': fn,
        'LastName': ln,
        'Age': age,
      });
    } catch (e) {
      print(e);
    }
  }

  void _read() async {
    DocumentSnapshot documentSnapshot;
    try {
      documentSnapshot =
          await firestore.collection('user').doc('OrMkyIoptZ0W54UvaL3W').get();
      print(documentSnapshot.data);
    } catch (e) {
      print(e);
    }
  }

  void _update() async {
    var age = _ageController.text;
    var fn = _fnController.text;
    var ln = _lnController.text;
    try {
      firestore.collection('user').doc('OrMkyIoptZ0W54UvaL3W').update({
        'FirstName': fn,
        'LastName': ln,
        'Age': age,
      });
    } catch (e) {
      print(e);
    }
  }

  void _delete() async {
    try {
      firestore.collection('user').doc('OrMkyIoptZ0W54UvaL3W').delete();
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Enter Your Age"),
            TextFormField(
                controller: _ageController,
                autofocus: false,
                decoration:
                    buildInputDecoration("Enter Your Age", Icons.calculate)),
            SizedBox(
              height: 20.0,
            ),
            Text("Enter Your FirstName"),
            TextFormField(
                controller: _fnController,
                autofocus: false,
                decoration:
                    buildInputDecoration("Enter Your FirstName", Icons.person)),
            SizedBox(
              height: 20.0,
            ),
            Text("Enter Your LastName"),
            TextFormField(
                controller: _lnController,
                autofocus: false,
                decoration:
                    buildInputDecoration("Enter Your LastName", Icons.person)),
            SizedBox(
              height: 20.0,
            ),
            longButtons("create", _create),
            SizedBox(
              height: 20.0,
            ),
            SizedBox(
              height: 20.0,
            ),
            longButtons("read", _read),
            SizedBox(
              height: 20.0,
            ),
            SizedBox(
              height: 20.0,
            ),
            longButtons("update", _update),
            SizedBox(
              height: 20.0,
            ),
            SizedBox(
              height: 20.0,
            ),
            longButtons("delete", _delete),
            SizedBox(
              height: 20.0,
            ),
          ],
        ),
      ),
    );
  }
}
